<?php

function UnsharpMask($img, $amount, $radius, $threshold)
{
    if ($amount > 500) {
        $amount = 500;
    }
    $amount = $amount * 0.016;
    if ($radius > 50) {
        $radius = 50;
    }
    $radius = $radius * 2;
    if ($threshold > 255) {
        $threshold = 255;
    }
    $radius = abs(round($radius));
    if ($radius == 0) {
        return $img;
    }
    $w = imagesx($img);
    $h = imagesy($img);
    $imgCanvas = imagecreatetruecolor($w, $h);
    $imgBlur = imagecreatetruecolor($w, $h);

    if (function_exists('imageconvolution')) {
        $matrix = array(
            array(
                1,
                2,
                1
            ),
            array(
                2,
                4,
                2
            ),
            array(
                1,
                2,
                1
            )
        );
        imagecopy($imgBlur, $img, 0, 0, 0, 0, $w, $h);
        imageconvolution($imgBlur, $matrix, 16, 0);
    } else {
        for ($i = 0; $i < $radius; $i++) {
            imagecopy($imgBlur, $img, 0, 0, 1, 0, $w - 1, $h);
            imagecopymerge($imgBlur, $img, 1, 0, 0, 0, $w, $h, 50);
            imagecopymerge($imgBlur, $img, 0, 0, 0, 0, $w, $h, 50);
            imagecopy($imgCanvas, $imgBlur, 0, 0, 0, 0, $w, $h);
            imagecopymerge($imgBlur, $imgCanvas, 0, 0, 0, 1, $w, $h - 1, 33.33333);
            imagecopymerge($imgBlur, $imgCanvas, 0, 1, 0, 0, $w, $h, 25);
        }
    }

    if ($threshold > 0) {
        for ($x = 0; $x < $w - 1; $x++) {
            for ($y = 0; $y < $h; $y++) {
                $rgbOrig = ImageColorAt($img, $x, $y);
                $aOrig = ($rgbOrig >> 24) & 0xFF;
                $rOrig = (($rgbOrig >> 16) & 0xFF);
                $gOrig = (($rgbOrig >> 8) & 0xFF);
                $bOrig = ($rgbOrig & 0xFF);

                $rgbBlur = ImageColorAt($imgBlur, $x, $y);
                $rBlur = (($rgbBlur >> 16) & 0xFF);
                $gBlur = (($rgbBlur >> 8) & 0xFF);
                $bBlur = ($rgbBlur & 0xFF);

                $rNew = (abs($rOrig - $rBlur) >= $threshold) ? max(0, min(255, ($amount * ($rOrig - $rBlur)) + $rOrig)) : $rOrig;
                $gNew = (abs($gOrig - $gBlur) >= $threshold) ? max(0, min(255, ($amount * ($gOrig - $gBlur)) + $gOrig)) : $gOrig;
                $bNew = (abs($bOrig - $bBlur) >= $threshold) ? max(0, min(255, ($amount * ($bOrig - $bBlur)) + $bOrig)) : $bOrig;

                if (($rOrig != $rNew) || ($gOrig != $gNew) || ($bOrig != $bNew)) {
                    $pixCol = ImageColorAllocateAlpha($img, $rNew, $gNew, $bNew, $aOrig);
                    ImageSetPixel($img, $x, $y, $pixCol);
                }
            }
        }
    } else {
        for ($x = 0; $x < $w; $x++) {
            for ($y = 0; $y < $h; $y++) {
                $rgbOrig = ImageColorAt($img, $x, $y);
                $aOrig = ($rgbOrig >> 24) & 0xFF;
                $rOrig = (($rgbOrig >> 16) & 0xFF);
                $gOrig = (($rgbOrig >> 8) & 0xFF);
                $bOrig = ($rgbOrig & 0xFF);

                $rgbBlur = ImageColorAt($imgBlur, $x, $y);
                $rBlur = (($rgbBlur >> 16) & 0xFF);
                $gBlur = (($rgbBlur >> 8) & 0xFF);
                $bBlur = ($rgbBlur & 0xFF);

                $rNew = ($amount * ($rOrig - $rBlur)) + $rOrig;

                if ($rNew > 255) {
                    $rNew = 255;
                } elseif ($rNew < 0) {
                    $rNew = 0;
                }

                $gNew = ($amount * ($gOrig - $gBlur)) + $gOrig;

                if ($gNew > 255) {
                    $gNew = 255;
                } elseif ($gNew < 0) {
                    $gNew = 0;
                }

                $bNew = ($amount * ($bOrig - $bBlur)) + $bOrig;

                if ($bNew > 255) {
                    $bNew = 255;
                } elseif ($bNew < 0) {
                    $bNew = 0;
                }

                $rgbNew = ($aOrig << 24) + ($rNew << 16) + ($gNew << 8) + $bNew;

                ImageSetPixel($img, $x, $y, $rgbNew);
            }
        }
    }

    imagedestroy($imgCanvas);
    imagedestroy($imgBlur);

    return $img;
}

function filter_grayscale($img)
{
    $imgw = imagesx($img);
    $imgh = imagesy($img);

    for ($i=0; $i<$imgw; $i++) {
        for ($j=0; $j<$imgh; $j++) {
            // get the rgb value for current pixel

            $rgb = ImageColorAt($img, $i, $j);

            // extract each value for r, g, b

            $rr = ($rgb >> 16) & 0xFF;
            $gg = ($rgb >> 8) & 0xFF;
            $bb = $rgb & 0xFF;

            // get the Value from the RGB value

            $g = round(($rr + $gg + $bb) / 3);

            // grayscale values have r=g=b=g

            $val = imagecolorallocate($img, $g, $g, $g);

            // set the gray value

            imagesetpixel($img, $i, $j, $val);
        }
    }

    return $img;
}

session_start();

define('ROOT_DIR', dirname(dirname(__FILE__)));
define('SRC_DIR', ROOT_DIR . '/data/images/');
define('DST_DIR', ROOT_DIR . '/data/cache/images/');

$vars            = array();
$vars['file']    = isset($_REQUEST['file']) ? $_REQUEST['file'] : '';
$vars['width']   = isset($_REQUEST['width']) ? intval($_REQUEST['width']) : null;
$vars['height']  = isset($_REQUEST['height']) ? intval($_REQUEST['height']) : null;
$vars['type']    = isset($_REQUEST['type']) ? $_REQUEST['type'] : 'png';
$vars['quality'] = isset($_REQUEST['quality']) ? intval($_REQUEST['quality']) : 9;
$vars['filter']  = isset($_REQUEST['filter']) ? $_REQUEST['filter'] : null;

$force = isset($_REQUEST['force']) ? $_REQUEST['force'] : false;

if ( preg_match('/Chrome/', $_SERVER['HTTP_USER_AGENT']) ) {
    $vars['type'] = 'webp';
}

$_SESSION['var'] = $vars;

$src_name = null;

if (is_dir(SRC_DIR) == false) {
    mkdir(SRC_DIR, 0777, true);
}
if (is_dir(DST_DIR) == false) {
    mkdir(DST_DIR, 0777, true);
}

if ($vars['width'] > 2148) {
    $src_name = SRC_DIR . '/blank.png';
    list($vars['width'], $vars['height']) = getimagesize($src_name);
} elseif ($vars['height'] > 1024) {
    $src_name = ROOT_DIR . '/blank.png';
    list($vars['width'], $vars['height']) = getimagesize($src_name);
} elseif (is_file(SRC_DIR . '/' . $vars['file'] . '.img')) {
    $src_name = SRC_DIR . '/' . $vars['file'] . '.img';
} elseif (is_file(SRC_DIR . '/' . $vars['file'] . '.png')) {
    $src_name = SRC_DIR . '/' . $vars['file'] . '.png';
} elseif (is_file(SRC_DIR . '/' . $vars['file'] . '.gif')) {
    $src_name = SRC_DIR . '/' . $vars['file'] . '.gif';
} elseif (is_file(SRC_DIR . '/' . $vars['file'] . '.jpg')) {
    $src_name = SRC_DIR . '/' . $vars['file'] . '.jpg';
} elseif (is_file(SRC_DIR . '/' . $vars['file'] . '.jpeg')) {
    $src_name = SRC_DIR . '/' . $vars['file'] . '.jpeg';
} elseif (is_file(SRC_DIR . '/' . $vars['file'] . '.webp')) {
    $src_name = SRC_DIR . '/' . $vars['file'] . '.webp';
}

if (empty($src_name)) {
    $src_name = SRC_DIR . '/blank.png';
}

//printf("Source: %s<hr/>", $src_name);

$dst_name = DST_DIR . '/' . basename($vars['file']) . '/' . md5(serialize($vars)) . '.' . $vars['type'];
//printf("Destination: %s<hr/>", $dst_name);

$info = @getimagesize($src_name);
$r    = empty($info) ? 1 : round($info[0] / $info[1], 2);
if ($vars['width'] == 0) {
    $vars['width'] = round($vars['height'] * $r);
}
if ($vars['height'] == 0) {
    $vars['height'] = round($vars['width'] / $r);
}

if ($vars['width'] == 0 || $vars['height'] == 0) {
    exit();
}

$refresh = false;
$src  = null;

if ($force || basename($src_name) == 'blank.gif' || @filemtime($src_name) >= @filemtime($dst_name)) {
    list($width, $height) = $info;
    $mime = $info['mime'];
    $ext  = strtolower(substr($src_name, strrpos($src_name, '.') + 1));

    try {
        switch ($mime) {
            case 'image/jpeg':
                $src = imagecreatefromjpeg($src_name);
                break;
            case 'image/gif':
                $src = imagecreatefromgif($src_name);
                break;
            case 'image/png':
                $src = imagecreatefrompng($src_name);
                break;
            case 'image/webp':
                $src = imagecreatefromwebp ($src_name);
                break;
        }

        $dst = imagecreatetruecolor($vars['width'], $vars['height']);
        $col = imagecolorallocatealpha($dst, 0, 0, 0, 127);
        imagefilledrectangle($dst, 0, 0, $vars['width'], $vars['height'], $col);
        imagealphablending($dst, false);
        imagesavealpha($dst, true);
    } catch (Exception $ex) {
    }

    if ($src) {
        list($width, $height) = getimagesize($src_name);
        $ratio = $width / $height;
        $new   = array();
        if ($ratio >= 1) {
            $new['width']  = $vars['width'];
            $new['height'] = ceil($new['width'] / $ratio);
        } else {
            $new['height'] = $vars['height'];
            $new['width']  = ceil($new['height'] * $ratio);
        }
        $new['x'] = ceil(($vars['width'] - $new['width']) / 2);
        $new['y'] = ceil(($vars['height'] - $new['height']) / 2);

        imagecopyresampled($dst, $src, $new['x'], $new['y'], 0, 0, $new['width'], $new['height'], $width, $height);

        $dst = UnsharpMask($dst, 60, 1, 3);

        if ($vars['filter']) {
            foreach (explode(',', $vars['filter']) as $filter) {
                if (function_exists('filter_' . $filter)) {
                    $dst = call_user_func('filter_' . $filter, $dst);
                }
            }
        }

        if (is_dir(dirname(dirname(dirname($dst_name)))) == false) {
            mkdir(dirname(dirname(dirname($dst_name))));
        }
        if (is_dir(dirname(dirname($dst_name))) == false) {
            mkdir(dirname(dirname($dst_name)));
        }
        if (is_dir(dirname($dst_name)) == false) {
            mkdir(dirname($dst_name));
        }

        switch (strtolower($vars['type'])) {
            case 'jpeg':
            case 'jpg':
                imagejpeg($dst, $dst_name, $vars['quality'] * 10);
                break;
            case 'png':
                imagepng($dst, $dst_name, $vars['quality']);
                break;
            case 'gif':
                imagegif($dst, $dst_name);
                break;
            case 'webp':
                imagewebp($dst, $dst_name);
                break;
        }

        $refresh = true;
    }
}

if (is_file($dst_name) == false) {
    $dst_name = $src_name;
}

$stamp = date('D, d M Y H:i:s', filemtime($dst_name)) . ' GMT';
$etag  = md5(serialize($vars)) . '-' . filemtime($dst_name);

if ($refresh == false && array_key_exists('HTTP_IF_MODIFIED_SINCE', $_SERVER) && $_SERVER['HTTP_IF_MODIFIED_SINCE'] == $stamp) {
    header('HTTP/1.1 304 Not Modified');

    switch (strtolower($vars['type'])) {
        case 'jpeg':
        case 'jpg':
            header('Content-Type: image/jpeg', true);
            break;
        case 'png':
            header('Content-Type: image/png', true);
            break;
        case 'gif':
            header('Content-Type: image/gif', true);
            break;
        case 'webp':
            header('Content-Type: image/webp', true);
            break;
    }

    header('Content-Length: ' . filesize($dst_name));
    header('Expires: ' . date('D, d M Y H:i:s', filemtime($dst_name) + (60 * 60 * 2)) . ' GMT', true);
    header('Last-Modified: ' . $stamp, true);
    header('ETag: ' . $etag, true);
    header('Cache-Control: public, max-age=' . (60 * 60 * 2) . ', s-maxage=' . (60 * 60 * 2), true);
} elseif ($refresh == false && array_key_exists('HTTP_IF_NONE_MATCH', $_SERVER) && $_SERVER['HTTP_IF_NONE_MATCH'] == $etag) {
    header('HTTP/1.1 304 Not Modified');

    switch (strtolower($vars['type'])) {
        case 'jpeg':
        case 'jpg':
            header('Content-Type: image/jpeg', true);
            break;
        case 'png':
            header('Content-Type: image/png', true);
            break;
        case 'gif':
            header('Content-Type: image/gif', true);
            break;
        case 'webp':
            header('Content-Type: image/webp', true);
            break;
    }

    header('Content-Length: ' . filesize($dst_name));
    header('Expires: ' . date('D, d M Y H:i:s', filemtime($dst_name) + (60 * 60 * 2)) . ' GMT', true);
    header('Last-Modified: ' . $stamp, true);
    header('ETag: ' . $etag, true);
    header('Cache-Control: public, max-age=' . (60 * 60 * 2) . ', s-maxage=' . (60 * 60 * 2), true);
} else {
    header('HTTP/1.1 200 OK');

    switch (strtolower($vars['type'])) {
        case 'jpeg':
        case 'jpg':
            header('Content-Type: image/jpeg', true);
            break;
        case 'png':
            header('Content-Type: image/png', true);
            break;
        case 'gif':
            header('Content-Type: image/gif', true);
            break;
        case 'webp':
            header('Content-Type: image/webp', true);
            break;
    }

    header('Content-Length: ' . filesize($dst_name));
    header('Expires: ' . date('D, d M Y H:i:s', filemtime($dst_name) + (60 * 60 * 2)) . ' GMT', true);
    header('Last-Modified: ' . $stamp, true);
    header('ETag: ' . $etag, true);
    header('Cache-Control: public, max-age=' . (60 * 60 * 2) . ', s-maxage=' . (60 * 60 * 2), true);

    echo (file_get_contents($dst_name));
}
exit();
