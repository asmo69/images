<?php

    $file = stripslashes($_GET['file']);
    if (empty($file)) {
        $file = 'balloons';
    }

?>
<html>
    <head>
    </head>
    <body>
            <ul>
                <li><a href="?file=balloons">balloons</a></li>
                <li><a href="?file=marbles">marbles</a></li>
                <li><a href="?file=peacock">peacock</a></li>
                <li><a href="?file=pencil">pencil</a></li>
            </ul>

            <center>
                
                <picture>
                    <source media="(max-width: 320px)" srcset="images.php?width=320&file=<?php echo($file) ?>&filter=grayscale, images.php?width=640&file=<?php echo($file) ?>&filter=grayscale 2x">
                    <source media="(max-width: 640px)" srcset="images.php?width=640&file=<?php echo($file) ?>, images.php?width=1024&file=<?php echo($file) ?> 2x">
                    <source media="(max-width: 800px)" srcset="images.php?width=800&file=<?php echo($file) ?>&filter=grayscale, images.php?width=1600&file=<?php echo($file) ?>&filter=grayscale 2x">
                    <source media="(min-width: 800px)" srcset="images.php?width=1024&file=<?php echo($file) ?>, images.php?width=2048&file=<?php echo($file) ?>">
                    <img src="images.php?width=1024&file=<?php echo($file) ?>" style="width: 100%;">
                </picture>
                
            <center>
    </body>
</html>
