if [ ! -f ./phpcs.phar ]; 
then
	curl -sL --output /tmp/phpcs.phar https://squizlabs.github.io/PHP_CodeSniffer/phpcs.phar; 
fi

(git diff --name-only && git diff --name-only --staged)  | egrep '\.(php|inc)$' | egrep -v '^external' | while read line; do if [ -f $line ]; then echo $line; fi done | xargs php /tmp/phpcs.phar -n --tab-width=4 --standard=psr2

exit 0
