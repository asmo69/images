Exec {
  path => ["/usr/bin", "/bin", "/usr/sbin", "/sbin", "/usr/local/bin", "/usr/local/sbin"]
}

# ----------------------------------------------------------------------
# Packages
# ----------------------------------------------------------------------

exec { "apt-get update":
    command => "/usr/bin/apt-get update -yq",
    timeout => 0
}
->
package { "python-software-properties":
    ensure => "present"
}
->
exec { "add-apt-repository -y ppa:ondrej/php":
    timeout => 0
}
->
exec { "apt-get upgrade":
    command => "/usr/bin/apt-get update -yq && /usr/bin/apt-get upgrade -yq",
    timeout => 0
}
->
package { ["whois", "build-essential", "vim", "emacs", "nano", "git", "curl", "wget", "telnet", "postfix", "memcached", "apache2", "php7.0", "php7.0-cli", "php7.0-curl", "php7.0-gd", "php7.0-mysql", "php7.0-mcrypt", "php7.0-mbstring", "php7.0-xml", "php7.0-xdebug", "php7.0-memcached", "php7.0-memcache", "libapache2-mod-php7.0", "mailutils", "libsasl2-2", "ca-certificates", "libsasl2-modules"]:
    ensure => latest
}
->

# ----------------------------------------------------------------------
# Configure Project
# ----------------------------------------------------------------------

file { "/vagrant":
    ensure => "directory",
    force => true,
    mode => "0777"
}
->
file { "/vagrant_data":
    ensure => "directory",
    force => true,
    mode => "0777"
}
->
file { "/vagrant_data/cache":
    ensure => "directory",
    force => true,
    mode => "0777",
    recurse => true
}
->


# ----------------------------------------------------------------------
# Configure apache
# ----------------------------------------------------------------------

exec { "a2enmod php7.0":
    unless => "apache2ctl -M | grep php7"
}
->
exec { "a2enmod rewrite":
    unless => "apache2ctl -M | grep rewrite"
}
->
exec { "a2enmod headers":
    unless => "apache2ctl -M | grep headers"
}
->
exec { "a2enmod expires":
    unless => "apache2ctl -M | grep expires"
}
->
exec { "a2enmod ssl":
    unless => "apache2ctl -M | grep ssl"
}
->
exec { "rm -rf /etc/apache2/sites-available/*":
}
->
exec { "rm -rf /etc/apache2/sites-enabled/*":
}
->
file { "/etc/apache2/ports.conf":
    ensure => "file",
    content =>"NameVirtualHost *:80

Listen 80
"
}
file { "/etc/apache2/sites-available/default.conf":
    ensure => "file",
    content => "<VirtualHost *:80>
    DocumentRoot /vagrant/public

    <Directory /vagrant/public>
        Options All
        AllowOverride All
        Require all granted
    </Directory>

    EnableSendfile off
    ServerSignature Off

    ErrorLog \${APACHE_LOG_DIR}/default-error.log
    CustomLog \${APACHE_LOG_DIR}/default-access.log combined
</VirtualHost>"
}
->
file { "/etc/apache2/sites-enabled/010-default.conf":
    ensure => "link",
    target => "/etc/apache2/sites-available/default.conf"
}
->


# ----------------------------------------------------------------------
# Configure Git
# ----------------------------------------------------------------------

file { "/home/vagrant/.git-prompt.sh":
    mode => "0755",
    content => "__git_ps1 ()
{
	PS1=\$(git branch 2>/dev/null | grep '*' | awk '{ print $2 }')
	if [ \"\$PS1\" != \"\" ];
	then
		echo \" (\$PS1)\"
	fi
}
"
}
->
file { "/home/vagrant/.bash_aliases":
    mode => "0755",
    content => "if [ -f ~/.git-prompt.sh ];
then
    source ~/.git-prompt.sh
    PS1=\"\\[\$BLUE\\]\\u\\[\$YELLOW\\] \\[\$YELLOW\\]\\w\\[\\033[m\\]\\[\$MAGENTA\\]\\$(__git_ps1)\\[\$WHITE\\]\\$ \"
fi
"
}
->

# ----------------------------------------------------------------------
# Restart Services
# ----------------------------------------------------------------------

exec { "service postfix restart":
}
->
exec { "service memcached restart":
}
->
exec { "service apache2 restart":
}
