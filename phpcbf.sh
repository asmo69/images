if [ ! -f ./phpcbf.phar ]; 
then
	curl -sL --output /tmp/phpcbf.phar https://squizlabs.github.io/PHP_CodeSniffer/phpcbf.phar 
fi

(git diff --name-only && git diff --name-only --staged)  | egrep '\.(php|inc)$' | egrep -v '^external' | while read line; do if [ -f $line ]; then echo $line; fi done | xargs php /tmp/phpcbf.phar -n --tab-width=4 --standard=psr2

exit 0

