Images
======

Requirements
------------

 - **Mac OS X or Linux** (Windows not tested)
 - **Vagrant >= 1.4**
 - **VirtualBox >= 4.3**



Overview
--------

This code shows a simple and example of using media queries and dynamically resizing images.

As you resize the browser the image shown on the screen will be resized. The image will be dynamically resized to avoid pixelation effects that can occur either from displaying an image that is too large or too small.

Images are resized on the fly and as such, the first time they are requested there is a short delay. However, the resized images are then cached and each subsequent request will receive the cached image with no delay.

Cached images will be refreshed after 2 hours or if the original source image is updated.

N.B. To better indicate the transition between media queries, the images alternate between colour and grayscale.


Getting Started
---------------

Clone the code into a local repository and run the command:

    vagrant up

You should now be able to access the site at the following URL:

    http://localhost:8080
